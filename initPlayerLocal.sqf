// Set groupID
f_script_setGroupIDs = [] execVM "Scripts\setGroupID\f_setGroupIDs.sqf";

// Set fireteam Color
f_script_setTeamColours = [] execVM "Scripts\setTeamColours\f_setTeamColours.sqf";

// Fireteam Member Markers
[] spawn f_fnc_SetLocalFTMemberMarkers;

// Group markers
f_script_setGroupMarkers = [] execVM "Scripts\groupMarkers\f_setLocalGroupMarkers.sqf";

// Fast Rope
_null = [] execVM "Scripts\fastrope\zlt_fastrope.sqf";

// Vehicle Crew Info
_null = [] execVM "scripts\crew\crew.sqf"; 