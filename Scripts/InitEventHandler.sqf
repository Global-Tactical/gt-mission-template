class Extended_Init_EventHandlers {
 class O_officer_F {
  init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Officer.sqf')";
 };
 class O_Soldier_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Rifleman.sqf')";
};
 class O_Soldier_lite_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Riflemanlite.sqf')";
};
 class O_Soldier_GL_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Grenadier.sqf')";
};
 class O_Soldier_AR_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Autorifleman.sqf')";
};
 class O_Soldier_SL_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\SquadLeader.sqf')";
};
 class O_Soldier_TL_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\TeamLeader.sqf')";
};
 class O_soldier_M_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Marksman.sqf')";
};
 class O_Soldier_LAT_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\RiflemanAT.sqf')";
};
 class O_medic_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\CombatLifeSaver.sqf')";
};
 class O_soldier_repair_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\RepairSpecialist.sqf')";
};
 class O_soldier_exp_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ExplosiveSpecialist.sqf')";
};
 class O_helipilot_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\HelicopterPilot.sqf')";
};
 class O_Soldier_A_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AmmoBearer.sqf')";
};
 class O_Soldier_AT_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\MissileSpecialistAT.sqf')";
};
 class O_Soldier_AA_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\MissileSpecialistAA.sqf')";
};
 class O_engineer_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Engineer.sqf')";
};
 class O_crew_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Crewman.sqf')";
};
 class O_Pilot_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Pilot.sqf')";
};
 class O_helicrew_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\HelicopterCrew.sqf')";
};
 class O_soldier_PG_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ParaTrooper.sqf')";
};
 class O_soldier_UAV_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\UAVOperator.sqf')";
};
 class O_diver_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AssaultDiver.sqf')";
};
 class O_diver_TL_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\DiverTeamLeader.sqf')";
};
 class O_diver_exp_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\DiverExplosiveSpecialist.sqf')";
};
 class O_spotter_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Spotter.sqf')";
};
 class O_sniper_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\Sniper.sqf')";
};
 class O_recon_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconScout.sqf')";
};
 class O_recon_M_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconMarksman.sqf')";
};
 class O_recon_LAT_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconScoutAT.sqf')";
};
 class O_recon_medic_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconParamedic.sqf')";
};
 class O_recon_exp_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconDemoSpecialist.sqf')";
};
 class O_recon_JTAC_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconJTAC.sqf')";
};
 class O_recon_TL_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\ReconTeamLeader.sqf')";
};
 class O_Soldier_AAR_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AsstAutorifleman.sqf')";
};
 class O_Soldier_AAT_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AsstMissileSpecialistAT.sqf')";
};
 class O_Soldier_AAA_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AsstMissileSpecialistAA.sqf')";
};
 class O_support_MG_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\GunnerHMG.sqf')";
};
 class O_support_GMG_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\GunnerGMG.sqf')";
};
 class O_support_Mort_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\GunnerMk6.sqf')";
};
 class O_support_AMG_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AsstGunnerHMGGMG.sqf')";
};
 class O_support_AMort_F {
 init = "_this call (compile preprocessFileLineNumbers 'Scripts\AI_Loadout_Manager\OPFor_Loadout\AsstGunnerMk6.sqf')";
};
};