// HMQ Teleport script

_unit = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_teleportTo = _this select 3;

if (Alive _teleportTo) then {
	_caller moveInCargo _teleportTo;
	player vehicleChat ["You have been deplayed at Mobile HQ"];
} else {
	player groupChat format ["Unable to deplay %1 to Mobile HQ", _unit];
};