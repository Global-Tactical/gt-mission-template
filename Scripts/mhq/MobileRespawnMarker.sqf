// Mobile Respawn Script
// By xNiMRoDx

// Wait for players to spawn
if (!isDedicated && (isNull player)) then
{
    waitUntil {sleep 3.0; !isNull player};
};

[] spawn {
	while {!isnil "MHQ1"} do {"Respawn_west_1" setMarkerPosLocal (getPos MHQ1); sleep 0.3;
	};
};

[] spawn {
	while {!isnil "MHQ2"} do {"Respawn_west_2" setMarkerPosLocal (getPos MHQ2); sleep 0.3;
	};
};