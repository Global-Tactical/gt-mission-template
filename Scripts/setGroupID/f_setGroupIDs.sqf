// GT - Set Group IDs
//Credit: xNiMRoDx & F3 mission framework.


// Nato GroupID's
_groups = [

["GrpNATO_CO","1st. CO -"],

["GrpNATO_ASL","1st. ASL -"],
["GrpNATO_A1","1st. A1 -"],
["GrpNATO_A2","1st. A2 -"],
["GrpNATO_A3","1st. A3 -"],

["GrpNATO_BSL","1st. BSL -"],
["GrpNATO_B1","1st. B1 -"],
["GrpNATO_B2","1st. B2 -"],
["GrpNATO_B3","1st. B3 -"],

["GrpNATO_CSL","1st. CSL -"],
["GrpNATO_C1","1st. C1 -"],
["GrpNATO_C2","1st. C2 -"],
["GrpNATO_C3","1st. C3 -"],

["GrpNATO_CAS1","1st. Talon1 -"],
["GrpNATO_CAS2","1st. Talon2 -"],

["GrpNATO_TH1","1st. Angel1 -"],
["GrpNATO_TH2","1st. Angel2 -"],


// CSAT GroupID's
["GrpCSAT_CO","1st. CO -"],

["GrpCSAT_ASL","1st. ASL -"],
["GrpCSAT_A1","1st. A1 -"],
["GrpCSAT_A2","1st. A2 -"],
["GrpCSAT_A3","1st. A3 -"],

["GrpCSAT_BSL","1st. BSL -"],
["GrpCSAT_B1","1st. B1 -"],
["GrpCSAT_B2","1st. B2 -"],
["GrpCSAT_B3","1st. B3 -"],

["GrpCSAT_CSL","1st. CSL -"],
["GrpCSAT_C1","1st. C1 -"],
["GrpCSAT_C2","1st. C2 -"],
["GrpCSAT_C3","1st. C3 -"]];

// SET GROUP IDS
// Execute setGroupID Function for all factions
{_x call f_fnc_setGroupID} forEach _groups;