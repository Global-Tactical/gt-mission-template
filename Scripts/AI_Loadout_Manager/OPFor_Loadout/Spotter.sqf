comment "Exported from Arsenal by GT_xNiMRoDx";
private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};
comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "U_O_GhillieSuit";
for "_i" from 1 to 5 do {_unit addItemToUniform "AGM_Bandage";};
_unit addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 4 do {_unit addItemToUniform "SmokeShell";};
for "_i" from 1 to 2 do {_unit addItemToUniform "9Rnd_45ACP_Mag";};
_unit addVest "MNP_Vest_RUMED_B";
for "_i" from 1 to 3 do {_unit addItemToVest "HandGrenade";};
for "_i" from 1 to 8 do {_unit addItemToVest "hlc_30Rnd_545x39_B_AK";};
_unit addGoggles "G_Bandanna_blk";

comment "Add weapons";
_unit addWeapon "hlc_rifle_aks74";
_unit addPrimaryWeaponItem "hlc_optic_kobra";
_unit addWeapon "hgun_ACPC2_F";
_unit addWeapon "Rangefinder";

comment "Add items";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_fadak";
_unit linkItem "ItemGPS";
_unit linkItem "NVGoggles_OPFOR";
