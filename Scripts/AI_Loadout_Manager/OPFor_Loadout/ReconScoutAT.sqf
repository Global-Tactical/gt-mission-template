comment "Exported from Arsenal by GT_xNiMRoDx";

private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};

comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "MNP_CombatUniform_RO2_Sh";
_unit addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 5 do {_unit addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 4 do {_unit addItemToUniform "SmokeShell";};
for "_i" from 1 to 3 do {_unit addItemToUniform "HandGrenade";};
_unit addVest "MNP_Vest_RU_T2";
for "_i" from 1 to 2 do {_unit addItemToVest "9Rnd_45ACP_Mag";};
for "_i" from 1 to 8 do {_unit addItemToVest "hlc_30Rnd_545x39_B_AK";};
_unit addBackpack "MNP_B_RU1_FP";
for "_i" from 1 to 2 do {_unit addItemToBackpack "RPG32_F";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "RPG32_HE_F";};
_unit addHeadgear "MNP_Boonie_RU_T";

comment "Add weapons";
_unit addWeapon "hlc_rifle_aks74u";
_unit addWeapon "launch_RPG32_F";
_unit addWeapon "hgun_ACPC2_F";
_unit addWeapon "Rangefinder";

comment "Add items";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_fadak";
_unit linkItem "ItemGPS";
_unit linkItem "NVGoggles_OPFOR";