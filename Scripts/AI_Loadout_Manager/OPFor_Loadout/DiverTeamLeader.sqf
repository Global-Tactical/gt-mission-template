comment "Exported from Arsenal by GT_xNiMRoDx";
private ["_unit"];
_unit = _this select 0;
IF(!local _unit) exitwith {};
comment "Remove existing items";
removeAllWeapons _unit;
removeAllItems _unit;
removeAllAssignedItems _unit;
removeUniform _unit;
removeVest _unit;
removeBackpack _unit;
removeHeadgear _unit;
removeGoggles _unit;

comment "Add containers";
_unit forceAddUniform "U_O_Wetsuit";
for "_i" from 1 to 5 do {_unit addItemToUniform "AGM_Bandage";};
_unit addItemToUniform "AGM_EarBuds";
_unit addItemToUniform "AGM_DeadManSwitch";
_unit addItemToUniform "AGM_DefusalKit";
_unit addItemToUniform "AGM_Clacker";
for "_i" from 1 to 4 do {_unit addItemToUniform "SmokeShell";};
for "_i" from 1 to 3 do {_unit addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {_unit addItemToUniform "9Rnd_45ACP_Mag";};
_unit addVest "V_RebreatherIR";
_unit addBackpack "MNP_B_RU2_FP";
for "_i" from 1 to 8 do {_unit addItemToBackpack "hlc_30Rnd_545x39_B_AK";};
for "_i" from 1 to 4 do {_unit addItemToBackpack "SmokeShell";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "SmokeShellRed";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "SmokeShellGreen";};
for "_i" from 1 to 2 do {_unit addItemToBackpack "SmokeShellBlue";};
_unit addHeadgear "MNP_Helmet_PAGST_RO";
_unit addGoggles "G_Combat";

comment "Add weapons";
_unit addWeapon "hlc_rifle_aks74u";
_unit addWeapon "hgun_ACPC2_F";
_unit addWeapon "Binocular";

comment "Add items";
_unit linkItem "ItemMap";
_unit linkItem "ItemCompass";
_unit linkItem "ItemWatch";
_unit linkItem "tf_fadak";
_unit linkItem "ItemGPS";
_unit linkItem "NVGoggles_OPFOR";
