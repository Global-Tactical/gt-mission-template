class fspectator
{
	file = "Scripts\spect";
	class CamInit{};
	class OnUnload{};
	class DrawTags{};
	class EventHandler{};
	class GetIcon{};
	class FreeCam{};
	class GetPlayers{};
	class ReloadModes{};
	class UpdateValues{};
	class HandleCamera{};
	class ToggleGUI{};
};