// Headless Client

if !(hasInterface or isServer) then {
  HeadlessVariable = true;
  publicVariable "HeadlessVariable";
};